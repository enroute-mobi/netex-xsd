<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns="http://www.netex.org.uk/netex" xmlns:netex="http://www.netex.org.uk/netex" xmlns:siri="http://www.siri.org.uk/siri" xmlns:xsd="http://www.w3.org/2001/XMLSchema" targetNamespace="http://www.netex.org.uk/netex" elementFormDefault="qualified" attributeFormDefault="unqualified" version="2.0" id="netex_fareDebit_support">
	<xsd:include schemaLocation="../../netex_framework/netex_genericFramework/netex_loggable_support.xsd"/>
	<!-- =============================================================== -->
	<xsd:annotation>
		<xsd:appinfo>
			<Metadata xmlns="http://www.govtalk.gov.uk/CM/gms-xs">
				<Aggregation>main schema</Aggregation>
				<Audience>e-service developers</Audience>
				<Contributor>V1.0 Christophe Duquesne</Contributor>
				<Contributor>Nicholas Knowles</Contributor>
				<Coverage>Europe</Coverage>
				<Creator>First drafted for NeTEx version 1.0 CEN TC278 WG3 SG9 Editor Nicholas Knowles.</Creator>
				<Date>
					<Created>2023-12-18</Created>
				</Date>
				<Date>
					<Modified>2023-12-18</Modified>
				</Date>
				<Description>
					<p>NeTEx is a European CEN standard for the exchange of Public Transport data including timetables.</p>
					<p>This sub-schema describes the FARE DEBIT types.</p>
				</Description>
				<Format>
					<MediaType>text/xml</MediaType>
					<Syntax>http://www.w3.org/2001/XMLSchema</Syntax>
					<Description>XML schema, W3C Recommendation 2001</Description>
				</Format>
				<Identifier>{http://www.netex.org.uk/schemas/1.0/xsd/netex_part_3/part3_FareDebits}netex_FareDebit_support.xsd</Identifier>
				<Language>[ISO 639-2/B] ENG</Language>
				<Publisher>Kizoom, 109-123 Clifton Street, London EC4A 4LD </Publisher>
				<Relation>
					<Requires>http://www.netex.org.uk/schemas/1.0/PATH/netex_prereqfile.xsd</Requires>
				</Relation>
				<Rights>Unclassified
					 <Copyright>CEN, Crown Copyright 2009-2023</Copyright>
				</Rights>
				<Source>
					<ul>
						<li>Derived from the Transmodel,   standards.</li>
					</ul>
				</Source>
				<Status>Version 1.0</Status>
				<Subject>
					<Category>Arts, recreation and travel, Tourism, Travel (tourism), Transport,
Air transport, Airports,
Ports and maritime transport, Ferries (marine),
Public transport, Bus services, Coach services, Bus stops and stations,
Rail transport, Railway stations and track, Train services, Underground trains,
Business and industry, Transport, Air transport , Ports and maritime transport, Public transport,
Rail transport, Roads and Road transport
</Category>
					<Project>CEN TC278 WG3 SG9.</Project>
				</Subject>
				<Title>NeTEx FARE DEBIT identifier types.</Title>
				<Type>Standard</Type>
			</Metadata>
		</xsd:appinfo>
		<xsd:documentation>NeTEx: FARE DEBIT identifier types.</xsd:documentation>
	</xsd:annotation>
	<!-- ==== FARE  DEBIT ======================================================== -->
	<xsd:complexType name="fareDebitRefs_RelStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a collection of one or more references to a FARE DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="oneToManyRelationshipStructure">
				<xsd:sequence>
					<xsd:element ref="FareDebitRef" maxOccurs="unbounded"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:simpleType name="FareDebitIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of a FARE DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="LogEntryIdType"/>
	</xsd:simpleType>
	<xsd:element name="FareDebitRef" type="FareDebitRefStructure" abstract="false" substitutionGroup="LogEntryRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a FARE DEBIT.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="FareDebitRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for Reference to a FARE DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="LogEntryRefStructure">
				<xsd:attribute name="ref" type="FareDebitIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a FARE DEBIT.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ==== OTHER DEBIT ======================================================== -->
	<xsd:simpleType name="OtherDebitIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of an OTHER DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="FareDebitIdType"/>
	</xsd:simpleType>
	<xsd:element name="OtherDebitRef" type="OtherDebitRefStructure" abstract="false" substitutionGroup="FareDebitRef">
		<xsd:annotation>
			<xsd:documentation>Reference to an OTHER DEBIT.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="OtherDebitRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for Reference to an OTHER DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="FareDebitRefStructure">
				<xsd:attribute name="ref" type="OtherDebitIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of an OTHER DEBIT.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ==== BOOKING DEBIT ======================================================== -->
	<xsd:simpleType name="BookingDebitIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of a BOOKING DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="FareDebitIdType"/>
	</xsd:simpleType>
	<xsd:element name="BookingDebitRef" type="BookingDebitRefStructure" abstract="false" substitutionGroup="FareDebitRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a BOOKING DEBIT.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="BookingDebitRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for Reference to a BOOKING DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="FareDebitRefStructure">
				<xsd:attribute name="ref" type="BookingDebitIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a BOOKING DEBIT.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ==== TRIP DEBIT ======================================================== -->
	<xsd:simpleType name="TripDebitIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of a TRIP DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="FareDebitIdType"/>
	</xsd:simpleType>
	<xsd:element name="TripDebitRef" type="TripDebitRefStructure" abstract="false" substitutionGroup="FareDebitRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a TRIP DEBIT.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="TripDebitRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for Reference to a TRIP DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="FareDebitRefStructure">
				<xsd:attribute name="ref" type="TripDebitIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a TRIP DEBIT.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ==== FARE PRODUCT SALES DEBIT ======================================================== -->
	<xsd:simpleType name="FareProductSaleDebitIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of a FARE PRODUCT SALES DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="FareDebitIdType"/>
	</xsd:simpleType>
	<xsd:element name="FareProductSaleDebitRef" type="FareProductSaleDebitRefStructure" abstract="false" substitutionGroup="FareDebitRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a FARE PRODUCT SALES DEBIT.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="FareProductSaleDebitRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for Reference to a FARE PRODUCT SALES DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="FareDebitRefStructure">
				<xsd:attribute name="ref" type="FareProductSaleDebitIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a FARE PRODUCT SALES DEBIT.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ==== OFFENCE DEBIT ======================================================== -->
	<xsd:simpleType name="OffenceDebitIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of a OFFENCE DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="FareDebitIdType"/>
	</xsd:simpleType>
	<xsd:element name="OffenceDebitRef" type="OffenceDebitRefStructure" abstract="false" substitutionGroup="FareDebitRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a OFFENCE DEBIT.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="OffenceDebitRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for Reference to a OFFENCE DEBIT.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="FareDebitRefStructure">
				<xsd:attribute name="ref" type="OffenceDebitIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a OFFENCE DEBIT.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
</xsd:schema>
