<?xml version="1.0" encoding="UTF-8"?>
<!-- 2007 03 23 Add repeating name -->
<xsd:schema xmlns="http://www.netex.org.uk/netex" xmlns:netex="http://www.netex.org.uk/netex" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:siri="http://www.siri.org.uk/siri" targetNamespace="http://www.netex.org.uk/netex" elementFormDefault="qualified" attributeFormDefault="unqualified" version="2.0" id="netex_deckPath_version">
	<xsd:include schemaLocation="../../netex_part_1/part1_ifopt/netex_ifopt_path_version.xsd"/>
	<xsd:include schemaLocation="netex_deckPath_support.xsd"/>
	<xsd:include schemaLocation="netex_deckPlan_support.xsd"/>
	<xsd:include schemaLocation="../netex_genericFramework/netex_path_version.xsd"/>
	<xsd:include schemaLocation="netex_environment_version.xsd"/>
	<xsd:include schemaLocation="netex_equipment_version.xsd"/>
	<xsd:include schemaLocation="../netex_genericFramework/netex_place_version.xsd"/>
	<!-- =====NB The following two files are in NETEX IFOPT part1 and should be factored into comm use at some point  ======================== -->
	<xsd:include schemaLocation="../../netex_part_1/part1_ifopt/netex_ifopt_equipmentAll.xsd"/>
	<xsd:include schemaLocation="../../netex_part_1/part1_ifopt/netex_ifopt_checkConstraint_version.xsd"/>
	<xsd:annotation>
		<xsd:appinfo>
			<Metadata xmlns="http://www.govtalk.gov.uk/CM/gms-xs">
				<Aggregation>main schema</Aggregation>
				<Audience>e-service developers</Audience>
				<Contributor>V1.0 Nicholas Knowles</Contributor>
				<Contributor>Roger Slevin [Roger.Slevin@dft.gsi.gov.uk]</Contributor>
				<Coverage>Europe</Coverage>
				<Creator>Created as W3C .xsd schema by Nicholas Knowles. as 1.0 XML schema </Creator>
				<Date>
					<Created>2024-01-17</Created>
				</Date>
				<Date><Modified>2024-02-18</Modified>Norway review comment - Add Window types.
				</Date>
				<Description>
					<Title>NeTEx Network Exchange - DECK PATH LINK types.</Title>
					<p>
					</p>
				</Description>
				<Format>
					<MediaType>text/xml</MediaType>
					<Syntax>http://www.w3.org/2001/XMLSchema</Syntax>
					<Description>XML schema, W3C Recommendation 2001</Description>
				</Format>
				<Identifier>{http://www.netex.org.uk/schemas/1.0/xsd/netex_part_1/part1_ifopt}netex_deckPathLink_version.xsd</Identifier>
				<Language>[ISO 639-2/B] ENG</Language>
				<Publisher>CEN TC278 SG6 and Department for Transport, Great Minster House, 76 Marsham Street, London SW1P 4DR</Publisher>
				<Relation>
					<Requires>http://www.netex.org.uk/schemas/1.0/ifopt/netex_ifopt_xxxxx.xsd</Requires>
				</Relation>
				<Rights>Unclassified
					 <Copyright>CEN, Crown Copyright 2009-2024</Copyright>
				</Rights>
				<Source>
					<ul>
						<li>Evolved from Transmodel standard.</li>
					</ul>
				</Source>
				<Status>Version 1.0</Status>
				<Subject>
					<Category>Arts, recreation and travel, Tourism, Travel (tourism), Transport,
Air transport, Airports,
Ports and maritime transport, Ferries (marine),
Public transport, Bus services, Coach services, Bus stops and stations,
Rail transport, Railway stations and track, Train services, Underground trains,
Business and industry, Transport, Air transport , Ports and maritime transport, Public transport,
Rail transport, Roads and Road transport
</Category>
					<Project>CEN TC278 WG3 SG9.</Project>
				</Subject>
				<Title>NeTEx Network Exchange DECK PATH LINK Types . </Title>
				<Type>Standard</Type>
			</Metadata>
		</xsd:appinfo>
		<xsd:documentation>NeTEx: DECK PATH LINK  and NAVIGATION PATH Model.</xsd:documentation>
	</xsd:annotation>
	<!-- ============================================== -->
	<xsd:group name="DeckPathGroup">
		<xsd:annotation>
			<xsd:documentation>Deck Path elemenst fro DECK</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="deckPathJunctions" type="deckPathJunctionRefs_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>PATH JUNCTIONs  for DECK</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="deckPathLinks" type="deckPathLinkRefs_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>PATH LINKs for DECK </xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="deckNavigationPaths" type="deckNavigationPaths_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>NAVIGATION PATHs for DECK </xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:group>
	<!-- ==== DECK PATH LINK ===================================================== -->
	<xsd:complexType name="deckPathLinkRefs_RelStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a list of DECK PATH LINKs.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="containmentAggregationStructure">
				<xsd:choice maxOccurs="unbounded">
					<xsd:element ref="GenericPathLinkRef"/>
					<xsd:element ref="DeckPathLink">
						<xsd:annotation>
							<xsd:documentation>PATH LINK for a DECK</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:choice>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="DeckPathLink" substitutionGroup="Link">
		<xsd:annotation>
			<xsd:documentation>A link within a DECK PLAN that represents a step in a possible route for pedestrians, wheelchair users or other out-of-vehicle passengers. +V2.0
</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:restriction base="DeckPathLink_VersionStructure">
					<xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="EntityInVersionGroup" minOccurs="0"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DataManagedObjectGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="LinkGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="GenericPathLinkGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="FixedPathLinkGroup"/>
							<xsd:group ref="DeckPathLinkComponentGroup"/>
							<xsd:group ref="DeckPathLinkGroup"/>
						</xsd:sequence>
					</xsd:sequence>
					<xsd:attribute name="id" type="DeckGenericPathLinkIdType" use="optional">
						<xsd:annotation>
							<xsd:documentation>Identifier of ENTITY.</xsd:documentation>
						</xsd:annotation>
					</xsd:attribute>
				</xsd:restriction>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:complexType name="DeckPathLink_VersionStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a DECK PATH LINK.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="GenericPathLink_VersionStructure">
				<xsd:sequence>
					<xsd:group ref="FixedPathLinkGroup"/>
					<xsd:group ref="DeckPathLinkComponentGroup"/>
					<xsd:group ref="DeckPathLinkGroup"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:group name="DeckPathLinkGroup">
		<xsd:annotation>
			<xsd:documentation>Elements of a DECK PATH LINK.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Label" type="MultilingualString" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Additional public label for the DECK PATH LINK</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="FromEquipableSpace" type="DeckPathLinkEndStructure">
				<xsd:annotation>
					<xsd:documentation>Start point of DECK PATH LINK.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="ToEquipableSpace" type="DeckPathLinkEndStructure">
				<xsd:annotation>
					<xsd:documentation>Start point of DECK PATH LINK.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:group>
	<xsd:group name="DeckPathLinkComponentGroup">
		<xsd:annotation>
			<xsd:documentation>DECK COMPONENT elements  of a DECK PATH LINK.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="DeckRef" minOccurs="0"/>
			<xsd:element ref="DeckLevelRef" minOccurs="0"/>
			<xsd:element ref="FareClass" minOccurs="0"/>
			<xsd:element ref="ClassOfUseRef" minOccurs="0"/>
			<xsd:element name="checkConstraints" type="checkConstraints_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Impediments to navigation from processes or barriers. For example security, check in etc.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:group ref="AllEquipmentGroup"/>
		</xsd:sequence>
	</xsd:group>
	<!-- ======================================================================= -->
	<xsd:complexType name="DeckPathLinkEndStructure">
		<xsd:annotation>
			<xsd:documentation>Beginning or end of a DECK PATH LINK, referencing a DECK COMPONENT or LOCATABLE SPOT. May be linked to a specific DECK LEVEL.  +V2.0</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="DeckRef" minOccurs="0"/>
			<xsd:element ref="DeckLevelRef" minOccurs="0"/>
			<xsd:element ref="EquipableSpaceRef" minOccurs="0"/>
			<xsd:element ref="DeckPathJunctionRef" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="DeckPointStructure">
		<xsd:annotation>
			<xsd:documentation>Valid endpoint on a DECK, either a DECK ENTRANCE, LOCATABLE SPOT</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="DeckRef" minOccurs="0"/>
			<xsd:element ref="DeckLevelRef" minOccurs="0"/>
			<xsd:choice>
				<xsd:element ref="DeckEntranceRef"/>
				<xsd:element ref="DeckSpaceRef"/>
				<xsd:element ref="DeckPathJunctionRef"/>
			</xsd:choice>
			<xsd:element ref="LocatableSpotRef" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
	<!-- =====DECK PATH JUNCTION ===================================================== -->
	<xsd:complexType name="deckPathJunctionRefs_RelStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a list of DECK PATH JUNCTIONs.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="strictContainmentAggregationStructure">
				<xsd:sequence>
					<xsd:element ref="DeckPathJunction" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>DECK PATH JUNCTION for a DECK.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="DeckPathJunction" substitutionGroup="Point">
		<xsd:annotation>
			<xsd:documentation>A designated point, inside a DECK SPACE , at which two or more DECK PATH LINKs may connect or branch. +V2.0</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:restriction base="DeckPathJunction_VersionStructure">
					<xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="EntityInVersionGroup" minOccurs="0"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DataManagedObjectGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="PointGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DeckPathJunctionGroup"/>
						</xsd:sequence>
					</xsd:sequence>
					<xsd:attribute name="id" type="DeckPathJunctionIdType" use="optional"/>
				</xsd:restriction>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:complexType name="DeckPathJunction_VersionStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a DECK PATH JUNCTION</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="GenericPathJunction_VersionStructure">
				<xsd:sequence>
					<xsd:group ref="DeckPathJunctionGroup"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:group name="DeckPathJunctionGroup">
		<xsd:annotation>
			<xsd:documentation>Elements of a DECK PATH JUNCTION.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:group ref="DeckElementPropertiesGroup">
				<xsd:annotation>
					<xsd:documentation>Elements of a SITE ELEMENTs.</xsd:documentation>
				</xsd:annotation>
			</xsd:group>
			<xsd:element name="Label" type="MultilingualString" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Additional Label of DECK PATH JUNCTION.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="EquipableSpaceRef"/>
		</xsd:sequence>
	</xsd:group>
	<xsd:group name="DeckPathJunctionSpotGroup">
		<xsd:annotation>
			<xsd:documentation>Elements of a DECK PATH LINK.</xsd:documentation>
		</xsd:annotation>
		<xsd:choice>
			<xsd:element ref="DeckEntranceRef"/>
			<xsd:element ref="DeckSpaceRef"/>
			<xsd:element ref="LocatableSpotRef"/>
		</xsd:choice>
	</xsd:group>
	<!-- ======= PLACES IN SEQUENCE ======================================= -->
	<xsd:complexType name="deckPlacesInSequence_RelStructure">
		<xsd:annotation>
			<xsd:documentation>A collection of one or more PLACEs in SEQUENCE.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="strictContainmentAggregationStructure">
				<xsd:sequence>
					<xsd:element ref="DeckPlaceInSequence" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>Deck place  traversed by  a DECK NAVIGATION PATH  in sequence. May be a DECK PLACE,  PATH JUNCTION or POINT.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="DeckPlaceInSequence" substitutionGroup="PointInLinkSequence">
		<xsd:annotation>
			<xsd:documentation>Point traversed by  a NAVIGATION PATH  in sequence. May be a PLACE PATH JUNCTION or EQUIPABLE SPACE.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:restriction base="DeckPlaceInSequence_VersionedChildStructure">
					<xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="EntityInVersionGroup" minOccurs="0"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="VersionedChildGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="PointInLinkSequenceGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DeckPlaceInSequenceGroup"/>
						</xsd:sequence>
					</xsd:sequence>
					<xsd:attribute name="id" type="DeckPlaceInSequenceIdType" use="optional">
						<xsd:annotation>
							<xsd:documentation>Identifier of ENTITY.</xsd:documentation>
						</xsd:annotation>
					</xsd:attribute>
				</xsd:restriction>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:complexType name="DeckPlaceInSequence_VersionedChildStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a DECK PLACE IN SEQUENCE.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="PointInLinkSequence_VersionedChildStructure">
				<xsd:sequence>
					<xsd:group ref="DeckPlaceInSequenceGroup"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:group name="DeckPlaceInSequenceGroup">
		<xsd:annotation>
			<xsd:documentation>Elements of DECK PLACE IN SEQUENCE.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="EquipableSpaceRef" minOccurs="0"/>
			<xsd:element ref="DeckPathJunction" minOccurs="0"/>
			<xsd:element name="BranchLevel" type="xsd:NMTOKEN" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Branching level of place.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="onwardLinks" type="pathLinksInSequence_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Onward links from this point.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:group>
	<!-- =====DECK NAVIGATION PATH =============================================== -->
	<xsd:complexType name="deckNavigationPaths_RelStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a list of DECK NAVIGATION PATHs.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="strictContainmentAggregationStructure">
				<xsd:sequence>
					<xsd:element ref="DeckNavigationPath" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>DECK NAVIGATION PATH for a DECK.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="DeckNavigationPath">
		<xsd:annotation>
			<xsd:documentation>A designated path between two places within a DECK PLAN. May include an ordered sequence of DECK PATH LINKs. +v2.0</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:restriction base="DeckNavigationPath_VersionStructure">
					<xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="EntityInVersionGroup" minOccurs="0"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DataManagedObjectGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="LinkSequenceGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DeckNavigationPathGroup"/>
						</xsd:sequence>
					</xsd:sequence>
					<xsd:attribute name="id" type="DeckNavigationPathIdType" use="optional">
						<xsd:annotation>
							<xsd:documentation>Identifier of ENTITY.</xsd:documentation>
						</xsd:annotation>
					</xsd:attribute>
				</xsd:restriction>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:complexType name="DeckNavigationPath_VersionStructure">
		<xsd:annotation>
			<xsd:documentation>Type for DECK NAVIGATION PATH.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="GenericNavigationPath_VersionStructure">
				<xsd:sequence>
					<xsd:group ref="DeckNavigationPathGroup"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:group name="DeckNavigationPathGroup">
		<xsd:annotation>
			<xsd:documentation>Elements of a DECK NAVIGATION PATH.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:group ref="DeckNavigationPathSummaryGroup"/>
			<xsd:group ref="DeckElementPropertiesGroup"/>
			<xsd:element name="AccessFeatureList" type="AccessFeatureListOfEnumerations" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Classification of Overall Accessibility of NAVIGATION PATH.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="DeckNavigationType" type="DeckNavigationPathTypeEnumeration" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Classification of DECK NAVIGATION..</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="deckPlacesInSequence" type="deckPlacesInSequence_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Ordered collection of References to STOP PLACE spaces Use for a branch path.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="pathLinksInSequence" type="pathLinksInSequence_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Ordered collection of References to PATH LINKs.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:group>
	<xsd:group name="DeckNavigationPathSummaryGroup">
		<xsd:annotation>
			<xsd:documentation>Elements of a DECK NAVIGATION PATH that summarise properties o the set of PATH LINKs. Will state the strictest constraint found . e.g. if any link forbids wheelchair, the DECK NAVIGATION PATH forbids wheelchair.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="From" type="DeckPathLinkEndStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Origin end of DECK NAVIGATION path. Only needed if detailed PATH LINKs are not given.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="To" type="DeckPathLinkEndStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Destination end of DECK NAVIGATION PATH. Only needed if detailed PATH LINKs not given.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:group ref="NavigationPathSummaryGroup"/>
		</xsd:sequence>
	</xsd:group>
	<!-- ======================================================================= -->
	<xsd:group name="DeckElementPropertiesGroup">
		<xsd:annotation>
			<xsd:documentation>Detailed properties of a DECK ELEMENT, these are simialr to properties of a SITE ELEMENT.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:group ref="EnvironmentPropertiesGroup"/>
			<xsd:element name="Presentation" type="PresentationStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Presentation defaults for DECK ELEMENT. +V2.0</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="facilities" type="serviceFacilitySets_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Facilities available at SITe.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:group>
	<!-- ======================================================================= -->
</xsd:schema>
