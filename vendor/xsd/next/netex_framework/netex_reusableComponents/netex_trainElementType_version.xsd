<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns="http://www.netex.org.uk/netex" xmlns:netex="http://www.netex.org.uk/netex" xmlns:siri="http://www.siri.org.uk/siri" xmlns:xsd="http://www.w3.org/2001/XMLSchema" targetNamespace="http://www.netex.org.uk/netex" elementFormDefault="qualified" attributeFormDefault="unqualified" version="2.0" id="netex_trainElement_version">
	<xsd:include schemaLocation="netex_trainElementType_support.xsd"/>
	<xsd:include schemaLocation="netex_trainElement_version.xsd"/>
	<xsd:annotation>
		<xsd:appinfo>
			<Metadata xmlns="http://www.govtalk.gov.uk/CM/gms-xs">
				<Aggregation>main schema</Aggregation>
				<Audience>e-service developers</Audience>
				<Contributor>V1.0 Christophe Duquesne</Contributor>
				<Contributor>Nicholas Knowles</Contributor>
				<Coverage>Europe</Coverage>
				<Creator>First drafted for NeTEx version 1.0 CEN TC278 WG3 SG9 Editor Nicholas Knowles.</Creator>
				<Date><Created>2024-01-11</Created>Transmodel - exten TRAIN and TRAIN ELEMENT tYPES 
				</Date>
				<Date><Modified>2024-01-11</Modified>Name Space changes
				</Date>
				<Description>
					<p>NeTEx is a European CEN standard for the exchange of Public Transport data including timetables.</p>
					<p>This sub-schema describes the TRAIN ELEMENT TYPE types</p>
				</Description>
				<Format>
					<MediaType>text/xml</MediaType>
					<Syntax>http://www.w3.org/2001/XMLSchema</Syntax>
					<Description>XML schema, W3C Recommendation 2001</Description>
				</Format>
				<Identifier>{http://www.netex.org.uk/schemas/1.0/xsd/netex_framework/netex_reusableComponents}netex_trainElement_version.xsd</Identifier>
				<Language>[ISO 639-2/B] ENG</Language>
				<Publisher>Kizoom Software Ltd, 16 High Holborn, London WC1V 6BX </Publisher>
				<Relation>
					<Requires>http://www.netex.org.uk/schemas/1.0/PATH/netex_prereqfile.xsd</Requires>
				</Relation>
				<Rights>Unclassified
					 <Copyright>CEN, Crown Copyright 2009-2024</Copyright>
				</Rights>
				<Source>
					<ul>
						<li>Derived from the Transmodel, VDV, TransXChange, NEPTUNE, BISON and Trident standards.</li>
					</ul>
				</Source>
				<Status>Version 1.0</Status>
				<Subject>
					<Category>Arts, recreation and travel, Tourism, Travel (tourism), Transport,
Air transport, Airports,
Ports and maritime transport, Ferries (marine),
Public transport, Bus services, Coach services, Bus stops and stations,
Rail transport, Railway stations and track, Train services, Underground trains,
Business and industry, Transport, Air transport , Ports and maritime transport, Public transport,
Rail transport, Roads and Road transport
</Category>
					<Project>CEN TC278 WG3 SG9.</Project>
				</Subject>
				<Title>NeTEx TRAIN ELEMENT Types.</Title>
				<Type>Standard</Type>
			</Metadata>
		</xsd:appinfo>
		<xsd:documentation>NeTEx TRAIN ELEMENT TYPEs Type specification</xsd:documentation>
	</xsd:annotation>
	<!-- ======================================================================= -->
	<xsd:group name="TrainElementTypesInFrameGroup">
		<xsd:annotation>
			<xsd:documentation>Elements for  TRAIN  TYPEs in frame.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="trainElementTypes" type="trainElementTypesInFrame_RelStructure" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>TRAIN ELEMENT TYPEs in frame _V2.0 in frame.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:group>
	<xsd:complexType name="trainElementTypesInFrame_RelStructure">
		<xsd:annotation>
			<xsd:documentation>Type for containment in frame of TRAIN ELEMENT TYPEs.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="containmentAggregationStructure">
				<xsd:choice maxOccurs="unbounded">
					<xsd:element ref="TrailingElementType"/>
					<xsd:element ref="TractiveElementType"/>
					<xsd:element ref="TrainElement"/>
				</xsd:choice>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ====== POWERED TRAIN =================================================== -->
	<xsd:element name="PoweredTrain" abstract="false" substitutionGroup="TrainType_">
		<xsd:annotation>
			<xsd:documentation>A TRAIN that is able to move under its own power. +v2.0</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:restriction base="PoweredTrain_VersionStructure">
					<xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="EntityInVersionGroup" minOccurs="0"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DataManagedObjectGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="TrainElementTypeGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="PoweredTrainGroup"/>
						</xsd:sequence>
					</xsd:sequence>
					<xsd:attribute name="id" type="PoweredTrainIdType"/>
				</xsd:restriction>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:complexType name="PoweredTrain_VersionStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a POWERED TRAIN.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="TrainElementType_VersionStructure">
				<xsd:sequence>
					<xsd:group ref="PoweredTrainGroup"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:group name="PoweredTrainGroup">
		<xsd:annotation>
			<xsd:documentation>Elements for a POWERED TRAIN.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence/>
	</xsd:group>
	<!-- ====== UNPOWERED TRAIN =================================================== -->
	<xsd:element name="UnpoweredTrain" abstract="false" substitutionGroup="TrainType_">
		<xsd:annotation>
			<xsd:documentation>A TRAIN that is able to move under its own power. +v2.0</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:restriction base="UnpoweredTrain_VersionStructure">
					<xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="EntityInVersionGroup" minOccurs="0"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DataManagedObjectGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="TrainElementTypeGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="UnpoweredTrainGroup">
								<xsd:annotation>
									<xsd:documentation>Elements for a UNPOWERED TRAIN.</xsd:documentation>
								</xsd:annotation>
							</xsd:group>
						</xsd:sequence>
					</xsd:sequence>
					<xsd:attribute name="id" type="UnpoweredTrainIdType"/>
				</xsd:restriction>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:complexType name="UnpoweredTrain_VersionStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a UNPOWERED TRAIN.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="TrainElementType_VersionStructure">
				<xsd:sequence>
					<xsd:group ref="UnpoweredTrainGroup"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:group name="UnpoweredTrainGroup">
		<xsd:annotation>
			<xsd:documentation>Elements for a UNPOWERED TRAIN.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence/>
	</xsd:group>
	<!-- ====== TRACTIVE ELEMENT TYPE =================================================== -->
	<xsd:element name="TractiveElementType" abstract="false">
		<xsd:annotation>
			<xsd:documentation>A TRAIN ELEMENT TYPE that is self-propelled, e.g., a locomotive or powered railcar. +v2.0</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:restriction base="TractiveElementType_VersionStructure">
					<xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="EntityInVersionGroup" minOccurs="0"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DataManagedObjectGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="TrainElementTypeGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="TractiveElementTypeGroup"/>
						</xsd:sequence>
					</xsd:sequence>
					<xsd:attribute name="id" type="TractiveElementTypeIdType"/>
				</xsd:restriction>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:complexType name="TractiveElementType_VersionStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a TRAIN ELEMENT.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="TrainElementType_VersionStructure">
				<xsd:sequence>
					<xsd:group ref="TractiveElementTypeGroup"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:group name="TractiveElementTypeGroup">
		<xsd:annotation>
			<xsd:documentation>Elements for a TRACTIVE ELEMENT TYPE.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence/>
	</xsd:group>
	<!-- ====== TRAILING ELEMENT TYPE =============================================== -->
	<xsd:element name="TrailingElementType" abstract="false">
		<xsd:annotation>
			<xsd:documentation>A TRAIN ELEMENT TYPE that cannot propel itself, e.g. a railway carriage, luggage van, passenger vehicle wagon, etc. +v2.0</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:restriction base="TrailingElementType_VersionStructure">
					<xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="EntityInVersionGroup" minOccurs="0"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="DataManagedObjectGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="TrainElementTypeGroup"/>
						</xsd:sequence>
						<xsd:sequence>
							<xsd:group ref="TrailingElementTypeGroup"/>
						</xsd:sequence>
					</xsd:sequence>
					<xsd:attribute name="id" type="TrailingElementTypeIdType"/>
				</xsd:restriction>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:complexType name="TrailingElementType_VersionStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a TRAILING ELEMENT TYPE.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="TrainElementType_VersionStructure">
				<xsd:sequence>
					<xsd:group ref="TrailingElementTypeGroup"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:group name="TrailingElementTypeGroup">
		<xsd:annotation>
			<xsd:documentation>Elements for a Trailing ELEMENT TYPE.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence/>
	</xsd:group>
	<!-- ======================================================================= -->
</xsd:schema>
