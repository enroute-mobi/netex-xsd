<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns="http://www.netex.org.uk/netex" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:netex="http://www.netex.org.uk/netex" targetNamespace="http://www.netex.org.uk/netex" elementFormDefault="qualified" attributeFormDefault="unqualified" version="2.0" id="netex_deckPath_support">
	<xsd:include schemaLocation="../netex_genericFramework/netex_path_support.xsd"/>
	<!-- ======================================================================= -->
	<xsd:annotation>
		<xsd:appinfo>
			<Metadata xmlns="http://www.govtalk.gov.uk/CM/gms-xs">
				<Aggregation>main schema</Aggregation>
				<Audience>e-service developers</Audience>
				<Coverage>Europe</Coverage>
				<Creator>First drafted for version 1.0 CEN TC278 WG3 SG6 Editor Nicholas Knowles.</Creator>
				<Date>
					<Created>2024-01-18</Created>
				</Date>
				<Date><Modified>2024-01-18</Modified>Move from Part1 Ifoft and bas on generic 
				</Date>
				<Description>
					<p>NeTEx - Network Exchange. This subschema defines DECK NAVIGATH PATH  types.</p>
				</Description>
				<Format>
					<MediaType>text/xml</MediaType>
					<Syntax>http://www.w3.org/2001/XMLSchema</Syntax>
					<Description>XML schema, W3C Recommendation 2001</Description>
				</Format>
				<Identifier>{http://www.netex.org.uk/schemas/1.0/xsd/netex_part_1/part1_ifopt}netex_ifopt_path_support.xsd</Identifier>
				<Language>[ISO 639-2/B] ENG</Language>
				<Publisher>Kizoom Software Ltd, 16 High Holborn, London WC1V 6BX</Publisher>
				<Rights>Unclassified
					 <Copyright>CEN, Crown Copyright 2009-2024</Copyright>
				</Rights>
				<Source>
					<ul>
						<li>Derived from the TRANSMODEL standards.</li>
					</ul>
				</Source>
				<Status>Version 1.0</Status>
				<Subject>
					<Category>Arts, recreation and travel, Tourism, Travel (tourism), Transport,
Air transport, Airports,
Ports and maritime transport, Ferries (marine),
Public transport, Bus services, Coach services, Bus stops and stations,
Rail transport, Railway stations and track, Train services, Underground trains,
Business and industry, Transport, Air transport , Ports and maritime transport, Public transport,
Rail transport, Roads and Road transport
</Category>
					<Project>CEN TC278 WG3 SG9.</Project>
				</Subject>
				<Title>NeTEx Network Exchange - DECK PATH identifier types.</Title>
				<Type>Standard</Type>
			</Metadata>
		</xsd:appinfo>
		<xsd:documentation>NeTEx: DECK PATH LINK identifier types for NeTEx.</xsd:documentation>
	</xsd:annotation>
	<!-- ===== DECK PATH LINK =============================================== -->
	<xsd:simpleType name="DeckGenericPathLinkIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of a DECK PATH LINK.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="GenericPathLinkIdType"/>
	</xsd:simpleType>
	<xsd:element name="DeckPathLinkRef" type="DeckPathLinkRefStructure" substitutionGroup="GenericPathLinkRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a DECK PATH LINK.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="DeckPathLinkRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a reference to a DECK PATH LINK.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="GenericPathLinkRefStructure">
				<xsd:attribute name="ref" type="DeckGenericPathLinkIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a DECK PATH LINK.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ====== DECK PATH JUNCTION =============================================== -->
	<xsd:simpleType name="DeckPathJunctionIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of a DECK PATH JUNCTION.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="GenericPathJunctionIdType"/>
	</xsd:simpleType>
	<xsd:element name="DeckPathJunctionRef" type="GenericPathJunctionRefStructure" substitutionGroup="GenericPathJunctionRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a DECK PATH JUNCTION.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="DeckPathJunctionRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a reference to a DECK PATH JUNCTION.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="GenericPathJunctionRefStructure">
				<xsd:attribute name="ref" type="GenericPathJunctionIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a PATH JUNCTION.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ===== DECK PLACE IN SEQUENCE ========================================== -->
	<xsd:simpleType name="DeckPlaceInSequenceIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of place step in a NAVIGATION PATH.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="PointInSequenceIdType"/>
	</xsd:simpleType>
	<xsd:element name="DeckPlaceInSequenceRef" type="DeckPlaceInSequenceRefStructure" substitutionGroup="PointInSequenceRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a DECK PLACE IN SEQUENCE. If given by context does not need to be stated.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="DeckPlaceInSequenceRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for reference to a DECK PLACE IN SEQUENCE.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="PointInSequenceRefStructure">
				<xsd:attribute name="ref" type="DeckPlaceInSequenceIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a DECK PLACE IN SEQUENCE.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ===== DECK NAVIGATION PATH ============================================ -->
	<xsd:complexType name="deckNavigationPathRefs_RelStructure">
		<xsd:annotation>
			<xsd:documentation>Type for a list of references to a DECK NAVIGATION PATH.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="oneToManyRelationshipStructure">
				<xsd:sequence>
					<xsd:element ref="DeckNavigationPathRef" maxOccurs="unbounded"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:simpleType name="DeckNavigationPathIdType">
		<xsd:annotation>
			<xsd:documentation>Type for identifier of a DECK NAVIGATION PATH.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="GenericNavigationPathIdType"/>
	</xsd:simpleType>
	<xsd:element name="DeckNavigationPathRef" type="DeckNavigationPathRefStructure" substitutionGroup="GenericNavigationPathRef">
		<xsd:annotation>
			<xsd:documentation>Reference to a DECK NAVIGATION PATH.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:complexType name="DeckNavigationPathRefStructure">
		<xsd:annotation>
			<xsd:documentation>Type for reference to a DECK NAVIGATION PATH.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:restriction base="GenericNavigationPathRefStructure">
				<xsd:attribute name="ref" type="DeckNavigationPathIdType" use="required">
					<xsd:annotation>
						<xsd:documentation>Identifier of a DECK NAVIGATION PATH.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:restriction>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:simpleType name="DeckNavigationPathTypeEnumeration">
		<xsd:annotation>
			<xsd:documentation>Allowed values for DECK NAVIGATION PATH type.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:NMTOKEN">
			<xsd:enumeration value="deckEntranceToDeckSpace"/>
			<xsd:enumeration value="deckEntranceToSpot"/>
			<xsd:enumeration value="spotToDeckSpace"/>
			<xsd:enumeration value="spotToDeckEntrance"/>
			<xsd:enumeration value="spotToSpot"/>
			<xsd:enumeration value="other"/>
		</xsd:restriction>
	</xsd:simpleType>
	<!-- ======================================================================= -->
</xsd:schema>
