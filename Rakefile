require "bundler/gem_tasks"

require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new do |t|
  t.pattern = './spec/**/*_spec.rb'
end

namespace :ci do
  RSpec::Core::RakeTask.new(:spec) do |t|
    t.pattern = './spec/**/*_spec.rb'
    t.rspec_opts = "--format RspecJunitFormatter --out test-results/rspec.xml"
  end
end

task ci: [ "ci:spec"]

namespace :xsd do
  def update(version)
    require 'tmpdir'

    Dir.mktmpdir do |temporary_dir|
      Dir.chdir(temporary_dir) do
        sh "wget https://github.com/NeTEx-CEN/NeTEx/archive/refs/heads/#{version}.zip"
        sh "unzip #{version}.zip"
      end

      sh "mv vendor/xsd/#{version} vendor/xsd/#{version}.old" if Dir.exist?("vendor/xsd/#{version}")
      sh "mv #{temporary_dir}/NeTEx-#{version}/xsd vendor/xsd/#{version}"
    end
  end

  namespace :update do
    desc "Replace vendor/xsd/master with current master from https://github.com/NeTEx-CEN/NeTEx/"
    task :master do
      update 'master'
    end

    desc "Replace vendor/xsd/next with current next branch from https://github.com/NeTEx-CEN/NeTEx/"
    task :next do
      update 'next'
    end

    desc "Replace/upgrade all vendor/xsd versions"
    task :all => [ :master, :next ]
  end
end

task default: :spec
