require "spec_helper"

RSpec.describe Netex::Xsd::Schema do
  subject(:schema) { described_class.new 'master' }

  describe "#file" do
    subject { schema.file }

    it "returns the vendor/xsd NeTEx_publication.xsd file" do
      expect(File.exist?(subject)).to be_truthy
    end
  end

  describe '#valid?' do
    subject { schema.valid? }

    context 'when schema file exists' do
      it { is_expected.to be_truthy }
    end

    context "when schema file doesn't exist" do
      let(:schema) { described_class.new 'wrong' }

      it { is_expected.to be_falsey }
    end
  end

  describe '.create' do
    subject { described_class.create name }

    context 'when resulting schema is valid' do
      let(:name) { 'master' }

      it 'returns the expected schema' do
        is_expected.to have_attributes(valid?: true)
      end
    end

    context 'when resulting schema is valid' do
      let(:name) { 'wrong' }

      it 'returns the expected schema' do
        is_expected.to be_nil
      end
    end
  end
end
