require "spec_helper"

RSpec.describe Netex::Xsd::Validator do
  it "validates a simple XML file" do
    subject.validate_file "spec/fixtures/examples/simple.xml"
    expect(subject.errors).to be_empty
  end

  it "validates a simple ZIP file" do
    subject.validate_file "spec/fixtures/examples/simple.zip"
    expect(subject.errors).to be_empty
  end

  it "validates a invalid XML file" do
    subject.validate_file "spec/fixtures/examples/invalid.xml"
    expect(subject.errors).to contain_exactly(an_object_having_attributes(level: 3))
  end

  context "with next XSD" do
    subject { described_class.new schema: Netex::Xsd::Schema.version('next') }

    it "validates a simple XML file" do
      subject.validate_file "spec/fixtures/examples/simple.xml"
      expect(subject.errors).to be_empty
    end

    it "validates a simple ZIP file" do
      subject.validate_file "spec/fixtures/examples/simple.zip"
      expect(subject.errors).to be_empty
    end
  end
end
