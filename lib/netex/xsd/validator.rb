require 'nokogiri'
require 'listen'
require 'zip'

module Netex
  module Xsd

    class Validator
      class NullLogger
        def unknown(*_args); end
        %i[fatal error warn info debug].each do |level|
          define_method(level) { |*args| }
          define_method("#{level}?") { false }
        end
      end

      def initialize(version: nil, schema: nil, logger: nil)
        @schema = schema || Schema.version(version) || Schema.default
        @logger = (logger || NullLogger.new)
      end

      attr_reader :logger, :schema

      def self.file_type(filename)
        File.binread(filename, 2) == "PK" ? :zip : :xml
      end

      def validate_file(file)
        if self.class.file_type(file) == :zip
          validate_zip file
        else
          validate file
        end
      end

      def validate_zip(input)
        Zip::File.open(input) do |zip_file|
          zip_file.glob("**/*.xml").each do |entry|
            validate entry.get_input_stream, filename: entry.name
          end
        end
      end

      def validate(input, filename: nil)
        log_filename = filename ? " #{filename}" : ""
        logger.debug "Inspect#{log_filename}"
        begin
          open_input(input) do |io|
            document = Nokogiri::XML::Document.parse(io, filename) { |config| config.strict }
            document_errors = schema.validate(document)

            logger.debug "Inspected#{log_filename}: #{document_errors.count} error(s)"
            errors.concat document_errors
          end
        rescue Nokogiri::XML::SyntaxError => e
          logger.debug "Inspected#{log_filename}: Invalid XML file"
          errors << e
        end
      end

      def open_input(input)
        if input.respond_to?(:read)
          yield input
        else
          File.open(input,'r') do |file|
            yield file
          end
        end
      end

      def valid?
        errors.empty?
      end

      def errors
        @errrors ||= []
      end

    end
  end
end
