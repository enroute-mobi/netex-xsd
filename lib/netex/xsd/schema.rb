module Netex::Xsd
  class Schema
    def self.default
      version 'master'
    end

    def self.version(name)
      return nil unless name
      (@schemas ||= Hash.new { |h,name| h[name] = Schema.create(name) })[name]
    end

    def self.create(name)
      schema = Schema.new(name)
      return nil unless schema.valid?

      schema
    end

    def initialize(version)
      @version = version
    end

    attr_reader :version

    def self.base_dir
      File.expand_path("../../../../vendor/xsd",__FILE__)
    end

    def directory
      File.join self.class.base_dir, version
    end

    def file
      File.join(directory, 'NeTEx_publication.xsd')
    end

    def valid?
      File.exist? file
    end

    def load
      Dir.chdir directory do
        File.open file, "r" do |file|
          Nokogiri::XML::Schema file
        end
      end
    end

    def validate(document)
      schema.validate(document)
    end

    def schema
      @schema ||= load
    end
  end
end
