require "netex/xsd/version"

module Netex
  module Xsd
    class Error < StandardError; end
    # Your code goes here...
  end
end

require 'netex/xsd/schema'
require 'netex/xsd/validator'
