lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "netex/xsd/version"

Gem::Specification.new do |spec|
  spec.name          = "netex-xsd"
  spec.version       = Netex::Xsd::VERSION
  spec.authors       = ["Alban Peignier"]
  spec.email         = ["contact@enroute.mobi"]

  spec.summary       = %q{Provide NeTEx XSD and related tools}
  spec.description   = %q{Validate our NeTEx files according to XSD schema}
  spec.homepage      = "https://bitbucket.org/enroute-mobi/netex-xsd"
  spec.license       = "Apache License Version 2.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"

  spec.add_runtime_dependency 'nokogiri'
  spec.add_runtime_dependency 'rubyzip'
  spec.add_runtime_dependency 'listen'
end
